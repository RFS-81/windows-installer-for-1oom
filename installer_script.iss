#define MyAppName "1oom"
#define MyAppVersion "1.1-beta"
#define MyAppPublisher "Max Fillinger"
#define MyAppExeName "1oom.exe"

[Messages]
SelectDirDesc=README!! This program requires files from Master of Orion 1 to run. Select the folder where you have installed Master of Orion 1; 1oom will be installed there. This will not change the original files.

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{1B2DDDE8-8A55-44A5-94DB-CFA56C023371}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={commonpf32}\GalaxyClient\Games\Master of Orion 1
DisableProgramGroupPage=yes
; The [Icons] "quicklaunchicon" entry uses {userappdata} but its [Tasks] entry has a proper IsAdminInstallMode Check.
UsedUserAreasWarning=no
; Remove the following line to run in administrative install mode (install for all users.)
PrivilegesRequired=lowest
OutputBaseFilename=mysetup
Compression=lzma
SolidCompression=yes
WizardStyle=modern
AppendDefaultDirName=no

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "1oom.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "SDL2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "SDL2_mixer.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "1oom.ico"; DestDir: "{app}"; Flags: ignoreversion
Source: "config\1oom_config_oldschool.txt"; DestDir: "{app}\1oom_oldschool\"; DestName: "config.txt"; Flags: ignoreversion
Source: "config\1oom_config_fixbugs.txt"; DestDir: "{app}\1oom_fixbugs\"; DestName: "config.txt"; Flags: ignoreversion
Source: "config\1oom_config_enhanced.txt"; DestDir: "{app}\1oom_enhanced\"; DestName: "config.txt"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autodesktop}\{#MyAppName} Oldschool"; IconFilename: "{app}\1oom.ico"; Filename: "{app}\{#MyAppExeName}"; Parameters: "-user 1oom_oldschool -c 1oom_oldschool/config.txt"; Tasks: desktopicon
Name: "{autodesktop}\{#MyAppName} Fixbugs"; IconFilename: "{app}\1oom.ico"; Filename: "{app}\{#MyAppExeName}"; Parameters: "-user 1oom_fixbugs -c 1oom_fixbugs/config.txt"; Tasks: desktopicon
Name: "{autodesktop}\{#MyAppName} Enhanced"; IconFilename: "{app}\1oom.ico"; Filename: "{app}\{#MyAppExeName}"; Parameters: "-user 1oom_enhanced -c 1oom_enhanced/config.txt"; Tasks: desktopicon